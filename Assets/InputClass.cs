﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// タップしたかどうか、タップ座標を返すクラス
/// </summary>
public class InputClass : MonoBehaviour
{
   
    public Vector2 ReturnRaycastVector2(Vector2 vectorpos) {

        var pos = vectorpos;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit))
        {

            pos.x = (int)hit.collider.gameObject.transform.localPosition.x;
            pos.y = (int)hit.collider.gameObject.transform.localPosition.y;

        }

        return pos;
    }















}
