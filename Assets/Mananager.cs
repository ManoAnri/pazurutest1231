﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Mananager : MonoBehaviour
{
    private const int cols = 8;
    private const int rows = 6;

    [SerializeField]
    private Pazurutest test;

    [SerializeField]
    private GameObject _PazuruBox;

    [SerializeField]
    private InputClass _InputClass;

    [SerializeField]
    private Particle _Particle;

    private GameObject[,] _Pazurus = new GameObject[cols, rows];

    private Pazurutest[,] _PazuruState = new Pazurutest[cols, rows];

    private GameObject Memoritest;

    private Pazurutest MemoriPazuri;

    private Vector2 MouseDown;

    private Vector2 MouseUp;

    private int[] Same_Block_Check_Direction_X = new int[] { 0, 1, 0, -1 };
    private int[] Same_Block_Check_Direction_Y = new int[] { 1, 0, -1, 0 };

    private int[] Same_5_Block_Cheack_Direction_X = new int[] { -1, -1, 0, 1, 1, 1, 0, -1 };
    private int[] Same_5_Block_Cheack_Direction_Y = new int[] { 0, 1, 1, 1, 0, -1, -1, -1 };

    private List<DestoryBlocks> _DestroyList = new List<DestoryBlocks>();

    private bool isCombo4 = false;
    private bool isDestroy = false;

    private static int Count; //ブロックを破壊した数


    //３つ以上揃う場合削除するブロックの位置を保存する
    public class DestoryBlocks
    {
        private readonly int x;
        private readonly int y;

        private eBoxType c_boxtype;

        //コンストラクタ
        public DestoryBlocks(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        //コンストラクタ2
        public DestoryBlocks(int x, int y, eBoxType c_boxtype)
        {

            this.x = x;
            this.y = y;
            this.c_boxtype = c_boxtype;

        }

        public int Getx() { return x; }
        public int Gety() { return y; }
        public eBoxType Getboxtype() { return c_boxtype; }
    }

    void Start()
    {
        for (var i = 0; i < cols; i++)
        {
            for (var k = 0; k < rows; k++)
            {
                NewPiaceInstance(k, i);
            }

        }

        Memoritest = Instantiate(_PazuruBox, new Vector3(10, 10, 10), Quaternion.identity);
        MemoriPazuri = Memoritest.GetComponent<Pazurutest>();
        MemoriPazuri.GetBox = eBoxType.BLUE;
        StartCoroutine(Start0());

    }

    IEnumerator Start0() 
    {
        Debug.Log("Start0");

        for (var i = 0; i < cols; i++)
        {
            for (var k = 0; k < rows; k++)
            {
                //NewPiaceInstance(k, i);
                var pos = _Pazurus[i, k].transform.position;
                //var y = pos.y;
                var yplus = pos.y - 9;

                while (pos.y >= yplus)
                {
                    pos.y--;
                    var posy = pos.y;
                    _Pazurus[i, k].transform.position = new Vector3(k, posy, 0);

                    //Debug.Log(pos.y);
                    yield return null;

                }
                if (i == cols && k == rows) { break; }

            }

        }
        yield return StartCoroutine(Start1());

    }

    IEnumerator Start1()
    {
        Debug.Log("Start1");

        while (true)
        {
            if (Input.GetMouseButton(0))
            {
                MouseDown = _InputClass.ReturnRaycastVector2(MouseDown);
                yield return StartupMouse();

            }
            yield return null;
        }

    }

    IEnumerator StartupMouse()
    {
        Debug.Log("StartupMouse");

        while (true)
        {
            if (Input.GetMouseButtonUp(0))
            {
                MouseUp = _InputClass.ReturnRaycastVector2(MouseUp);

                var xa = (int)MouseUp.x - MouseDown.x;
                var ya = (int)MouseUp.y - MouseDown.y;

                if (xa == 0 || xa == 1 || xa == -1)
                {
                    if (ya == 0 || ya == 1 || ya == -1) yield return Exchange_Block();
                }
                else { yield return Start1(); }

            }
            yield return null;
        }

    }


    IEnumerator Exchange_Block()
    {
        Debug.Log("Exchange_Block()");

        MemoriPazuri.GetBox = _PazuruState[(int)MouseDown.y, (int)MouseDown.x].GetBox;
        yield return null;

        _PazuruState[(int)MouseDown.y, (int)MouseDown.x].GetBox = _PazuruState[(int)MouseUp.y, (int)MouseUp.x].GetBox;
        yield return null;

        _PazuruState[(int)MouseUp.y, (int)MouseUp.x].GetBox = MemoriPazuri.GetBox;
        yield return null;

        if (isCombo4 = Conbo4_Exchange_Check(_DestroyList,_PazuruState[(int)MouseDown.y, (int)MouseDown.x].GetBox, _PazuruState[(int)MouseUp.y, (int)MouseUp.x].GetBox)) 
        {
            isCombo4 = true;
            CheckDestroy((int)MouseDown.x, (int)MouseDown.y, _DestroyList);
            CheckDestroy((int)MouseUp.x, (int)MouseUp.y, _DestroyList);

        }
        yield return null;
        yield return ComboCheck();
    }

    private bool Conbo4_Exchange_Check(List<DestoryBlocks> destroylist,params eBoxType[] pazuru1) {

        bool isconbo4 = false;

        for (var i = 0; i < pazuru1.Length; i++) {

            if (pazuru1[i] == eBoxType.DESTROY4 || pazuru1[i] == eBoxType.DESTROY5)
            { 
              isconbo4 = true; 
                         
            }

        }
        return isconbo4;
    }

    private void CheckDestroy(int x1, int y1,List<DestoryBlocks> blocks)
    {
        if (_PazuruState[y1, x1].GetBox == eBoxType.DESTROY4 || _PazuruState[y1, x1].GetBox == eBoxType.DESTROY5) 
        {
            blocks.Add(new DestoryBlocks(x1, y1));            
        }

    }

    IEnumerator ComboCheck()
    {
        Debug.Log("Combo4()");

        if (isCombo4)
        {
            foreach (var i in _DestroyList) {

                if (_PazuruState[i.Gety(), i.Getx()].GetBox == eBoxType.DESTROY4)
                {
                    DestroyCheak(i.Gety(), i.Getx(), Same_Block_Check_Direction_X, Same_Block_Check_Direction_Y);

                }
                if (_PazuruState[i.Gety(), i.Getx()].GetBox == eBoxType.DESTROY5)
                {
                    DestroyCheak(i.Gety(), i.Getx(), Same_5_Block_Cheack_Direction_X, Same_5_Block_Cheack_Direction_Y);

                }           
            }

            _DestroyList.Clear();
            isDestroy = true;
            isCombo4 = false;
        }
        else { isDestroy = false; }
        yield return null;
        yield return Check();
    }

    private void DestroyCheak(int i, int k, int[] arrayx, int[] arrayy)  
    {
        for (var p = 0; p < arrayx.Length; p++)
        {
            int px = k + arrayx[p];
            int py = i + arrayy[p];

            if (!(px >= 0 && px < rows && py >= 0 && py < cols)) continue;

            _PazuruState[py, px].GetBox = eBoxType.EMPTY;
        }
        _PazuruState[i, k].GetBox = eBoxType.EMPTY;
    }

    //１つ１つブロックの色を確認して、３つ以上同じ色が続いたらその色を緑にする
    IEnumerator Check()
    {
        Debug.Log("Check()");
       
        for (var i = 0; i < cols; i++)
        {
            for (var k = 0; k < rows; k++)
            {      
                for (var p = 0; p < Same_Block_Check_Direction_X.Length; p++)
                {
                    bool _CanTurn = false;

                    _DestroyList.Clear();
                    
                    eBoxType nowtype = _PazuruState[i, k].GetBox;

                    _DestroyList.Add(new DestoryBlocks(k, i, _PazuruState[i, k].GetBox));

                    int pazurux = k;
                    int pazuruy = i;

                    while (true)
                    {
                        pazurux += Same_Block_Check_Direction_X[p];
                        pazuruy += Same_Block_Check_Direction_Y[p];

                        if (!(pazurux >= 0 && pazurux < rows && pazuruy >= 0 && pazuruy < cols)) break;

                        if (_PazuruState[pazuruy, pazurux].GetBox == nowtype)
                        {
                            _DestroyList.Add(new DestoryBlocks(pazurux, pazuruy, _PazuruState[pazuruy, pazurux].GetBox));

                            int pazuruxplus = pazurux + Same_Block_Check_Direction_X[p];
                            int pazuruyplus = pazuruy + Same_Block_Check_Direction_Y[p];

                            if (_DestroyList.Count >= 3 && !(pazuruxplus >= 0 && pazuruxplus < rows && pazuruyplus >= 0 && pazuruyplus < cols))
                            {                               
                                _CanTurn = true;                               
                                break;
                            }
                            else { continue; }

                        }
                        else
                        {                            
                            if (_DestroyList.Count >= 3)
                            {                                
                                _CanTurn = true;
                                break;
                            }
                            break;
                        }

                    }

                    if (_CanTurn)
                    {
                        isDestroy = true;
                        if (nowtype != eBoxType.EMPTY) 
                        {
                            Count++;
                            Debug.Log("Count" + Count);

                        }
                        yield return new WaitForSeconds(0.3f);                                               
                        Pazuru_Combo_Count(_DestroyList, _DestroyList.Count);
                    }

                }

            }

        }
        yield return null;

        if (isDestroy)
        {           
            yield return DownBlock2();
            
        }
        else { yield return Start1(); }    
    }


    private void Pazuru_Combo_Count(List<DestoryBlocks> destroylist, int listcount)
    {
        Debug.Log("Pazuru_Combo_Count");

        if (listcount == 4)
        {
            foreach (var i in destroylist)
            {
                if (i == destroylist.First())
                {                  
                    _PazuruState[i.Gety(), i.Getx()].GetBox = eBoxType.DESTROY4;
                    var t = Instantiate(_Particle, new Vector3(i.Getx(), i.Gety(), 0), transform.rotation);
                    t.GetComponent<Particle>().GetParticle_Color = Color.white;
                }
                else
                { 
                    _PazuruState[i.Gety(), i.Getx()].GetBox = eBoxType.EMPTY;     
                }
            }
        }
        else if (listcount == 5)
        {
            foreach (var i in destroylist)
            {
                if (i == destroylist.First())
                {
                    _PazuruState[i.Gety(), i.Getx()].GetBox = eBoxType.DESTROY5;
                    var t = Instantiate(_Particle, new Vector3(i.Getx(), i.Gety(), 0), transform.rotation);
                    t.GetComponent<Particle>().GetParticle_Color = Color.white;

                }
                else 
                { 
                    _PazuruState[i.Gety(), i.Getx()].GetBox = eBoxType.EMPTY;                  
                }
            }

        }
        else
        {
            foreach (var i in destroylist)
            {
                _PazuruState[i.Gety(), i.Getx()].GetBox = eBoxType.EMPTY;

                if (i == destroylist.First()) 
                {
                    var t = Instantiate(_Particle, new Vector3(i.Getx(), i.Gety(), 0), transform.rotation);
                    t.GetComponent<Particle>().GetParticle_Color = Color.white;
                }
          
            }
        }

    }

    //↑に詰める作業
    IEnumerator DownBlock2()
    {
        Debug.Log("DownBlock2");

        for (var i = 0; i < cols; i++)
        {
            for (var k = 0; k < rows; k++)
            {
                if (_PazuruState[i, k].GetBox == eBoxType.EMPTY)
                {
                    int pazurux = k;
                    int pazuruy = i;

                    while (pazurux >= 0 && pazurux < rows && pazuruy >= 0 && pazuruy < cols)
                    {
                        var testpazuru = _PazuruState[pazuruy, pazurux].GetBox;

                        if (testpazuru != eBoxType.EMPTY)
                        {
                            _PazuruState[i, k].GetBox = testpazuru;
                            _PazuruState[pazuruy, pazurux].GetBox = eBoxType.EMPTY;
                            yield return new WaitForSeconds(0.1f);
                            break;

                        }
                        pazurux += Same_Block_Check_Direction_X[0];
                        pazuruy += Same_Block_Check_Direction_Y[0];
                    }

                }

            }

        }
        isDestroy = false;
        yield return null;
        yield return DownBlock3();

    }

    IEnumerator DownBlock3()
    {
        Debug.Log("DownBlock3");
        _DestroyList.Clear();
        for (var i = 0; i < cols; i++)
        {
            for (var k = 0; k < rows; k++)
            {
                if (_PazuruState[i, k].GetBox == eBoxType.EMPTY)
                {                   
                    yield return new WaitForSeconds(0.2f);
                    Destroy(_Pazurus[i, k]);                 
                    _DestroyList.Add(new DestoryBlocks(k, i));
                    
                }
            }
        }
        yield return DownBlock4();
    }

    IEnumerator DownBlock4()
    {
        foreach (var i in _DestroyList)
        {           
            NewPiaceInstance(i.Getx(), i.Gety());
            yield return null;
            var pos = _Pazurus[i.Gety(), i.Getx()].transform.position;
            var yplus = pos.y - 9;

            while (pos.y >= yplus)
            {
                pos.y--;
                var posy = pos.y;
                _Pazurus[i.Gety(), i.Getx()].transform.position = new Vector3(i.Getx(), posy, 0);

                //Debug.Log(pos.y);
                yield return null;
            }
          
            yield return new WaitForSeconds(0.2f);
        }
        _DestroyList.Clear();

        yield return null;
        yield return Check();

    }

    public void NewPiaceInstance(int x, int y)
    {
        var y_plus = y + 10;       
        _Pazurus[y, x] = Instantiate(_PazuruBox, new Vector3(x, y+10, 0), Quaternion.identity);

        _PazuruState[y, x] = _Pazurus[y, x].GetComponent<Pazurutest>();

        var r = Random.Range(0, (int)eBoxType.BLACK + 1);

        _PazuruState[y, x].GetBox = (eBoxType)r;
       
    }
}

