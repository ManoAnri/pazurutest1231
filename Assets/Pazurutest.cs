﻿using System.Collections;
using UnityEngine;

public enum eBoxType
{
    RED,
    BLUE,
    GREEN,
    YELLOW,
    WHITE,
    BLACK,
    EMPTY,
    DESTROY4,
    DESTROY5,
    FALL,
}

public class Pazurutest : MonoBehaviour
{

    [SerializeField]
    private MeshRenderer _PazuruMesh;

    [SerializeField]
    GameObject _Part;

    private Animator _animator = null;
    
    private Color red  = new Color(255,0,0);

    private Color green = new Color(0, 255, 0);

    private Color blue = new Color(0, 0, 255);

    private Color yellow = new Color(255, 255, 0);

    private Color white = new Color(255, 255, 255);

    private Color black = new Color(0, 0, 0);

    private Color combo4 = new Color(0, 255, 255);

    private Color combo5 = new Color(128, 0, 128);

    private Color fall = new Color(128, 128, 0);

    [SerializeField]
    private eBoxType _BoxType;

    public eBoxType GetBox
    {
        get => _BoxType;
        set
        {                       
            _BoxType = value;
            ChangeColor();
        }

    }

     private void Awake()
    {
        _animator = this.GetComponent<Animator>();

    }

    private void ChangeColor() {

        switch (GetBox) {

            case eBoxType.BLUE:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(blue));               
                break;
            case eBoxType.RED:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(red));               
                break;
            case eBoxType.YELLOW:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(yellow));                
                break;
            case eBoxType.GREEN:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(green));
                break;
            case eBoxType.WHITE:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(white));
                break;
            case eBoxType.BLACK:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(black));
                break;
            case eBoxType.DESTROY4:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(combo4));
                break;
            case eBoxType.DESTROY5:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(combo5));
                break;
            case eBoxType.FALL:
                this.GetComponent<MeshRenderer>().enabled = true;
                StartCoroutine(StartCol(fall));
                break;
            case eBoxType.EMPTY:
                this.GetComponent<MeshRenderer>().enabled = false;
                break;

           
            default:
                break;

        }

    }

    IEnumerator StartCol( Color color) {

        _animator.SetBool("Rotate", true);
        yield return new WaitForSeconds(0.5f);
        //var t = Instantiate(_Part, transform.position, transform.rotation);
        //t.GetComponent<Particle>().GetParticle_Color = Color.white;
        yield return new WaitForSeconds(0.5f);
        _animator.SetBool("Rotate", false);
        //Destroy(t);
        _PazuruMesh.GetComponent<MeshRenderer>().material.color = color;

    }

    IEnumerator DestroyCol() {

        
        var t = Instantiate(_Part, transform.position, transform.rotation);
        yield return new WaitForSeconds(0.3f);
        t.GetComponent<Particle>().GetParticle_Color = Color.red;
        yield return new WaitForSeconds(0.3f);
        Destroy(t);

    }

   




}
